<?php
declare(strict_types=1);
namespace App\Lpp\Entity\Factory;

use App\Lpp\Entity\Item;
use StdClass;

/**
 * Item factory class - simple builder class for constructing Brand object by given data
 *
 * @package App\Lpp\Entity\Factory
 */
class ItemFactory
{
    /**
     * @param StdClass $jsonObj
     *
     * @return Item
     */
    public static function buildFromJson(StdClass $jsonObj) : Item
    {
        $prices = [];

        foreach ($jsonObj->prices as $price) {
            $prices[] = PriceFactory::buildFromJson((object) $price);
        }

        return new Item(
            $jsonObj->name,
            $jsonObj->url,
            $prices
        );
    }
}