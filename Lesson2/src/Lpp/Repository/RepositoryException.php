<?php
declare(strict_types=1);
namespace App\Lpp\Repository;

use \Exception;

/**
 * Class RepositoryException
 *
 * Thrown by repository.
 *
 * @package App\Lpp\Repository
 */
class RepositoryException extends Exception
{
    /**
     * @param Exception $e
     *
     * @return RepositoryException
     */
    public static function buildForInfrastructure(Exception $e) : self
    {
        return new self('Infrastrucute Exception: ' . $e->getMessage(), $e->getCode(), $e);
    }
}