<?php
declare(strict_types=1);
namespace App\Lpp\Service;

use App\Lpp\Application\BrandCommand;
use App\Lpp\Infrastructure\JsonFileReader;
use App\Lpp\Repository\ItemJsonRepository;
use \InvalidArgumentException;


/**
 * Simple factory class for BrandService creation
 *
 * @package App\Lpp\Service
 */
class BrandServiceFactory
{
    /** @var string */
    private $path;

    /**
     * BrandServiceFactory constructor.
     *
     * @param string $readPath
     */
    public function __construct(string $readPath) {
        $this->path = $readPath;
    }

    /**
     * @param string $type
     *
     * @return BrandServiceInterface
     */
    public function getBrandService(string $type) : BrandServiceInterface
    {
        switch($type) {
            case BrandCommand::TYPES[0]:
                return new UnorderedBrandService(
                    new ItemService(
                        new ItemJsonRepository(
                            new JsonFileReader(
                                $this->path
                            )
                        )
                    )
                );
                break;
            case BrandCommand::TYPES[1]:
                return new NameOrderedBrandService(
                    new ItemService(
                        new ItemJsonRepository(
                            new JsonFileReader(
                                $this->path
                            )
                        )
                    )
                );
                break;
            default:
                throw new InvalidArgumentException();
        }
    }
}
