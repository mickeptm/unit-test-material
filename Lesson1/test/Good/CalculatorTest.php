<?php

namespace Xzsoftware\Test\Good;

use PHPUnit\Framework\TestCase;
use Xzsoftware\Good\Calculator;
use Xzsoftware\Math;

class CalculatorTest extends TestCase
{
    /**
     * @test
     */
    public function saveValueToMemoryWithIntegerReturnsSameInteger()
    {
        /* Mockups */
        $mock = $this->prophesize(Math::class);
        $mock->power(1,2)->willReturn(1);
        $math = $mock->reveal();

        /* Test logic */
        $calc = new Calculator($math);
        $calc->saveValueToMemory(10);

        /* Asserts */
        self::assertEquals(10, $calc->readValueFromMemory());
    }
}