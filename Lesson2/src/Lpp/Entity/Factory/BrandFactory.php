<?php
declare(strict_types=1);
namespace App\Lpp\Entity\Factory;

use App\Lpp\Entity\Brand;
use App\Lpp\Entity\Validation\ValidationException;
use StdClass;

/**
 * Brand factory class - simple builder class for constructing Brand object by given data
 *
 * @package App\Lpp\Entity\Factory
 */
class BrandFactory
{
    /**
     * @param StdClass $jsonObj
     *
     * @return Brand
     */
    public static function buildFromJson(StdClass $jsonObj) : Brand
    {
        $items = [];

        foreach ($jsonObj->items as $item) {
            try {
                $items[] = ItemFactory::buildFromJson((object) $item);
            } catch (ValidationException $exception) {}
        }

        return new Brand($jsonObj->name, $jsonObj->description, $items);
    }
}