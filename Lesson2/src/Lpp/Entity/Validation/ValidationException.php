<?php
declare(strict_types=1);
namespace App\Lpp\Entity\Validation;

use \Exception;

/**
 * ValidationException - should be thrown by any validator
 *
 * @package App\Lpp\Entity\Validation
 */
class ValidationException extends Exception
{
    const INVALID_VALUE_CODE = 1001;

    /**
     * @param string $error
     *
     * @return ValidationException
     */
    public static function buildWithError(string $error) : self
    {
        return new self('Validation failed: ' . $error, self::INVALID_VALUE_CODE);
    }
}