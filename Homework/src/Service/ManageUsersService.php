<?php
declare(strict_types=1);

namespace App\Service;

use App\Command\CreateUserCommand;
use App\Command\UpdateUserCommand;
use App\DTO\User;
use App\Repository\UserRepository;

class ManageUsersService
{
    /** @var UserRepository */
    private $repository;

    /**
     * ManageUsersService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CreateUserCommand $createUserCommand
     * @return User
     *
     * @throws \Exception
     */
    public function createUser(CreateUserCommand $createUserCommand): User
    {
        $this->validateUser($createUserCommand);

        return $this->repository->createUser($createUserCommand);
    }

    /**
     * @param UpdateUserCommand $updateUserCommand
     * @return User
     *
     * @throws \Exception
     */
    public function updateUser(UpdateUserCommand $updateUserCommand): User
    {
        $this->validateUser($updateUserCommand);

        return $this->updateUser($updateUserCommand);
    }

    /**
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function deleteUser(int $id): bool
    {
        if ($id < 1) {
            throw new \Exception('Invalid id');
        }

        return $this->repository->deleteUser($id);
    }

    /**
     * @param User $user
     * @throws \Exception
     */
    private function validateUser(User $user)
    {
        if (strlen($user->getName()) < 3) {
            throw new \Exception('Invalid name', 400);
        }

        $pos = strpos($user->getEmail(), '@');

        if (is_bool($pos) || $pos < 2) {
            throw new \Exception('Invalid email', 400);
        }
    }
}