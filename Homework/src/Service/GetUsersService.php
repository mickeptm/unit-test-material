<?php
declare(strict_types=1);

namespace App\Service;

use App\DTO\User;
use App\Repository\UserRepository;

class GetUsersService
{
    /** @var UserRepository */
    private $repository;

    /**
     * GetUsersService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $userId
     * @return User
     * @throws \Exception
     */
    public function getUser(int $userId): User
    {
        if ($userId < 1) {
            throw new \Exception('Invalid id');
        }

        return $this->repository->getUser($userId);
    }

    /**
     * @return array
     */
    public function getUsersCollection(): array
    {
        return $this->repository->getUsersCollection();
    }
}