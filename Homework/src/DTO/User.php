<?php
declare(strict_types=1);

namespace App\DTO;

use Symfony\Component\HttpFoundation\Request;

class User
{
    /** @var int */
    private $id;
    /** @var string */
    private $name;
    /** @var string */
    private $email;

    /**
     * User constructor.
     * @param string $name
     * @param string $email
     * @param int|null $id
     */
    public function __construct(string $name, string $email, int $id = null)
    {
        if ($id) {
            $this->id = $id;
        }
        $this->name = $name;
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function hasId()
    {
        return !is_null($this->id);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function toJson()
    {
        $data = [
            'name' => $this->getName(),
            'email' => $this->getEmail()
        ];

        if ($this->hasId()) {
            $data['id'] = $this->getId();
        }

        return json_encode($data);
    }
}