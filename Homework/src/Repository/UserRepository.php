<?php
declare(strict_types=1);

namespace App\Repository;

use App\Infrastructure\DataBase;
use App\DTO\User;

class UserRepository
{
    /** @var DataBase */
    private $db;

    /**
     * UserRepository constructor.
     * @param DataBase $db
     */
    public function __construct(DataBase $db)
    {
        $this->db = $db;
    }

    /**
     * @param int $id
     * @return User
     */
    public function getUser(int $id): User
    {
        $sql = $this->db->prepare('select id, name, email from USERS where id = :id');

        $sql->bindParam('id', $id);
        $sql->execute();
        $result = $sql->fetch();

        return new User($result['name'], $result['email'], $result['id']);
    }

    /**
     * @return array
     */
    public function getUsersCollection(): array
    {
        $sql = $this->db->prepare('select id, name, email from USERS');
        $sql->execute();
        $result = $sql->fetchAll();
        $collection = [];
        foreach($result as $res) {
            $collection[] = new User($res['name'], $res['email'], $res['id']);
        }

        return $collection;
    }

    /**
     * @param User $user
     * @return User
     */
    public function updateUser(User $user): User
    {
        $sql = $this->db->prepare('update USERS set name = :name, email = :email where id = :id');

        $sql->bindParam('id', $user->getId());
        $sql->bindParam('email', $user->getEmail());
        $sql->bindParam('name', $user->getName());
        $sql->execute();

        return $this->getUser($user->getId());
    }

    /**
     * @param User $user
     * @return User
     */
    public function createUser(User $user): User
    {
        $sql = $this->db->prepare('insert into USERS (name, email) values (:name, :email);');

        $sql->bindParam('email', $user->getEmail());
        $sql->bindParam('name', $user->getName());
        $sql->execute();

        return $this->getUser((int) $this->db->lastInsertId());
    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteUser(int $id): bool
    {
        $sql = $this->db->prepare('delete from USERS where id = :id;');

        $sql->bindParam('id', $id);
        return $sql->execute();
    }
}