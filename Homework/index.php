<?php
declare(strict_types=1);

include 'vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use App\Infrastructure\DataBase;

global $database;
$database = new DataBase('user', 'passwd', []);

$req = Request::createFromGlobals();
$controller = new \App\Controller\CRUDController();

switch($req->getMethod()) {
    case "GET":
        $id = $req->query->get('id', null);
        if ($id) {
            $resp = $controller->read($req);
        } else {
            $resp = $controller->list($req);
        }
        break;
    case "POST":
        $resp = $controller->create($req);
        break;
    case "PUT":
        $resp = $controller->update($req);
        break;
    case "DELETE":
        $resp = $controller->delete($req);
        break;
    default:
        $resp = new \Symfony\Component\HttpFoundation\Response('ERROR', 500);
        break;
}

$resp->send();

