<?php
declare(strict_types=1);
include 'vendor/autoload.php';

use App\Lpp\Application\BrandApplicationService;
use App\Lpp\Service\BrandServiceFactory;
use App\Lpp\Application\BrandCommand;

$dataDir = __DIR__ . '/data/';

$command = new BrandCommand('unordered', 1315475, 'winter');

$appService = new BrandApplicationService(
    new BrandServiceFactory($dataDir)
);

echo nl2br($appService->getBrandsString($command));
echo '<br ><br >' . "\n\n";
echo nl2br($appService->getItemsString($command));
