<?php
declare(strict_types=1);
namespace App\Lpp\Application;

use \Exception;

/**
 * Application Layer Exception for Application services
 *
 * @package App\Lpp\Application
 */
class AplicationException extends Exception
{
    const INVALID_COMMAND = 1001;

    /**
     * @return AplicationException
     */
    public static function buildInvalidCommand() : self
    {
        return new self('Invalid command given', self::INVALID_COMMAND);
    }
}