<?php
declare(strict_types=1);

namespace App\Command;

use App\DTO\User;
use Symfony\Component\HttpFoundation\Request;

class CreateUserCommand extends User
{
    public function __construct(string $name, string $email)
    {
        parent::__construct($name, $email);
    }

    /**
     * @param Request $request
     * @return CreateUserCommand
     */
    public static function buildFromRequest(Request $request): self
    {
        return new self(
            (string) $request->request->get('name', ''),
            (string) $request->request->get('email', '')
        );
    }
}
