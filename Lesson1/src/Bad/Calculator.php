<?php

namespace Xzsoftware\Bad;

use Xzsoftware\Math;

class Calculator
{
    /** @var Math */
    private $mathLib;
    /** @var null */
    private $memory = null;

    /**
     * @param $value
     */
    public function saveValueToMemory($value)
    {
        $this->memory = $value;
    }

    /**
     * @return null
     */
    public function readValueFromMemory()
    {
        return $this->memory;
    }

    private function loadMathLib()
    {
        $this->mathLib = new Math();
    }

    /**
     * @param $sign
     * @param $a
     * @param null $b
     * @return int|mixed
     */
    public function calculate($sign, $a, $b = null)
    {
        if (!($this->mathLib instanceof Math)) {
            $this->loadMathLib();
        }

        if (is_null($b)) {
            $b = $this->readValueFromMemory();
        }

        switch ($sign) {
            case '+':
                return $this->add($a, $b);
                break;
            case '-':
                return $this->subtract($a, $b);
                break;
            case '%':
                return $this->modulo($a, $b);
                break;
            case '^':
                return $this->power($a, $b);
                break;
            case "\/":
                $res = $this->mathLib->switcher([$a, $b - 1]);
                return $this->modulo($res[0] * $this->power($b, $a), $res[1]);
        }
    }

    /**
     * @param $a
     * @param $b
     * @return mixed
     */
    private function add($a, $b)
    {
        return $a + $b;
    }

    /**
     * @param $a
     * @param $b
     * @return mixed
     */
    private function subtract($a, $b)
    {
        return $a - $b;
    }

    /**
     * @param $value
     * @param $power
     * @return int
     */
    private function power($value, $power)
    {
        return $this->mathLib->power($value, $power);
    }

    /**
     * @param $value
     * @param $modulo
     * @return int
     */
    private function modulo($value, $modulo)
    {
        return $this->mathLib->modulo($value, $modulo);
    }
}