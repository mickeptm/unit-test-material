<?php
declare(strict_types=1);
namespace App\Lpp\Entity;

/**
 * Interface OrderableInterface
 * It forces orderable method that returns key (string) for order
 *
 * @package App\Lpp\Entity
 */
interface OrderableInterface
{
    /**
     * @return string
     */
    public function getOrderKey() : string;
}