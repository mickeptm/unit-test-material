<?php
declare(strict_types=1);
namespace App\Lpp\Entity\Validation;

/**
 * UrlValidator to proper validate of url string.
 * It could be refactored for full scale validation mechanizm
 * but it was keepd simple for purpouse of this task and logic simplification
 *
 * @package App\Lpp\Entity\Validation
 */
class UrlValidator
{
    /**
     * Static validator for simple validation of URL
     *
     * @param string $url
     *
     * @return bool
     */
    public static function validateUrl(string $url) : bool
    {
        $regex = '#[-a-zA-Z0-9@:%_\+.~\#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~\#?&//=]*)?#si';

        return (bool) preg_match($regex, $url);
    }
}
