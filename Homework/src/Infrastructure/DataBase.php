<?php
declare(strict_types=1);

namespace App\Infrastructure;
use \PDO;

class DataBase extends PDO
{
    public function __construct(string $username, string $passwd, array $options)
    {
        $dsn = 'mysql:dbname=testdb;host=127.0.0.1';

        parent::__construct($dsn, $username, $passwd, $options);
    }
}