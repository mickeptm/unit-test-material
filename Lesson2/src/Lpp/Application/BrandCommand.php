<?php
declare(strict_types=1);
namespace App\Lpp\Application;

/**
 * BrandCommand class that keeps data from different type of requests (is passed to Application Services)
 *
 * @package App\Lpp\Application
 */
class BrandCommand
{
    const TYPES = ['unordered', 'nameOrdered'];

    /** @var string  */
    private $type;

    /** @var int  */
    private $collectionId;

    /** @var string  */
    private $collectionName;

    /**
     * BrandCommand constructor.
     *
     * @param string $type
     * @param int $collectionId
     * @param string $collectionName
     *
     * @throws AplicationException
     */
    public function __construct(string $type, int $collectionId, string $collectionName)
    {
        if (!in_array($type, self::TYPES)) {
            throw AplicationException::buildInvalidCommand();
        }

        $this->type = $type;
        $this->collectionId = $collectionId;
        $this->collectionName = $collectionName;
    }

    /**
     * @return string
     */
    public function getType() : string
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getCollectionId() : int
    {
        return $this->collectionId;
    }

    /**
     * @return string
     */
    public function getCollectionName() : string
    {
        return $this->collectionName;
    }

}