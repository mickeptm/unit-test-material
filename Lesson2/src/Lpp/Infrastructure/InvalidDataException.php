<?php
declare(strict_types=1);
namespace App\Lpp\Infrastructure;

use \Exception;

/**
 * Invalid Data Exception, is thrown when we encounter problems with data
 *
 * @package App\Lpp\Infrastructure
 */
class InvalidDataException extends Exception
{
    const INVALID_JSON_CODE = 1001;

    /**
     * @return InvalidDataException
     */
    public static function buildForInvalidJson() : self
    {
        return new self('Invalid JSON string', self::INVALID_JSON_CODE);
    }
}