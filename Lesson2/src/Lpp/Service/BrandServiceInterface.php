<?php
declare(strict_types=1);
namespace App\Lpp\Service;

use App\Lpp\Entity\Item;
use App\Lpp\Entity\Brand;

/**
 * The implementation is responsible for resolving the id of the collection from the
 * given collection name.
 *
 * Second responsibility is to sort the returning result from the item service in whatever way.
 *
 */
interface BrandServiceInterface
{
     /**
     * @param string $collectionName Name of a collection to search for
     *
     * @return Item[]
     */
    public function getItemsForCollection(string $collectionName) : array;

    /**
     * @param string $collectionName
     *
     * @return Brand[]
     */
    public function getBrandsForCollection(string $collectionName) : array;

    /**
     * @param ItemServiceInterface $ItemService
     *
     * @return void
     */
    public function setItemService(ItemServiceInterface $ItemService) : void;
}
