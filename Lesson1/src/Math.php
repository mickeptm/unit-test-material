<?php

namespace Xzsoftware;

class Math
{
    public function power($value, $power)
    {
        if ($power == 0) {
            return 1;
        }

        if ($power == 1) {
            return $value;
        }

        $result = $value;

        for ($i = 1; $i < $power; $i++) {
            $result *= $value;
        }

        return $result;
    }

    public function modulo($value, $modulo)
    {
        return $value % $modulo;
    }

    public function switcher(array $arr)
    {
        $y = array_pop($arr);
        $x = array_pop($arr);

        $x ^= $y ^= ($y ^= $x);

        return [$x * 2, $y - 3];
    }
}