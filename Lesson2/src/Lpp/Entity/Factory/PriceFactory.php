<?php
declare(strict_types=1);
namespace App\Lpp\Entity\Factory;

use App\Lpp\Entity\Price;
use DateTime;
use StdClass;

/**
 * Price factory class - simple builder class for constructing Brand object by given data
 *
 * @package App\Lpp\Entity\Factory
 */
class PriceFactory
{
    /**
     * @param stdClass $jsonObj
     *
     * @return Price
     */
    public static function buildFromJson(stdClass $jsonObj) : Price
    {
        return new Price(
            $jsonObj->description,
            $jsonObj->priceInEuro,
            new DateTime($jsonObj->arrival),
            new DateTime($jsonObj->due)
        );
    }
}