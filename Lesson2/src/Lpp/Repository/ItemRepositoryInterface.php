<?php
declare(strict_types=1);
namespace App\Lpp\Repository;

use App\Lpp\Entity\Brand;

/**
 * Interface ItemRepositoryInterface - forced what method repository for Items needs to implement to be repository
 *
 * @package App\Lpp\Repository
 */
interface ItemRepositoryInterface
{
    /**
     * @param integer $id
     *
     * @return Brand[]
     */
    public function loadCollection(int $id) : array;

    /**
     * @param string $name
     *
     * @return Brand[]
     */
    public function loadCollectionByName(string $name) : array;
}