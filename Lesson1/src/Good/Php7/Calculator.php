<?php
declare(strict_types=1);

namespace Xzsoftware\Good\Php7;

use Xzsoftware\Math;

class Calculator
{
    /** @var int */
    private $memory;
    /** @var Math */
    private $mathLib = null;

    /**
     * Calculator constructor.
     * @param Math $mathLib
     */
    public function __construct(Math $mathLib)
    {
        $this->mathLib = $mathLib;
    }

    /**
     * @param $value
     */
    public function saveValueToMemory(int $value)
    {
        $this->memory = $value;
    }

    /**
     * @return int
     * @throws \Exception
     */
    public function readValueFromMemory() : int
    {
        if (is_null($this->memory)) {
            throw new \Exception();
        }

        return $this->memory;
    }

    /**
     * @param int $a
     * @param int|null $b
     * @return int
     */
    public function add(int $a, int $b = null) : int
    {
        if (is_null($b)) {
            $b = $this->readValueFromMemory();
        }

        return $a + $b;
    }

    /**
     * @param int $a
     * @param int|null $b
     * @return int
     */
    public function subtract(int $a, int $b = null) : int
    {
        if (is_null($b)) {
            $b = $this->readValueFromMemory();
        }

        return $a - $b;
    }

    /**
     * @param int $value
     * @param int|null $power
     * @return int
     */
    public function power(int $value, int $power = null) : int
    {
        if (is_null($power)) {
            $modulo = $this->readValueFromMemory();
        }

        return $this->mathLib->power($value, $power);
    }

    /**
     * @param int $value
     * @param int|null $modulo
     * @return int
     */
    public function modulo(int $value, int $modulo = null) : int
    {
        if (is_null($modulo)) {
            $modulo = $this->readValueFromMemory();
        }

        return $this->mathLib->modulo($value, $modulo);
    }
}