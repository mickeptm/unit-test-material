<?php
declare(strict_types=1);
namespace App\Lpp\Repository;

use App\Lpp\Entity\Factory\BrandFactory;
use App\Lpp\Infrastructure\HddException;
use App\Lpp\Infrastructure\InvalidDataException;
use App\Lpp\Infrastructure\JsonFileReader;
use App\Lpp\Entity\Brand;

/**
 * Data repository that reads from hard drive,
 * repository implementation solve problem of using any other data source,
 * it can be simply replaced by any other repository that implements specified interface
 *
 * @package App\Lpp\Repository
 */
class ItemJsonRepository implements ItemRepositoryInterface
{
    /** @var JsonFileReader */
    private $reader;

    /**
     * ItemJsonRepository constructor.
     *
     * @param JsonFileReader $fileReader
     */
    public function __construct(JsonFileReader $fileReader)
    {
        $this->reader = $fileReader;
    }

    /**
     * @param int $id
     *
     * @return Brand[]
     * @throws RepositoryException
     */
    public function loadCollection(int $id) : array
    {
        try {
            $data = $this->reader->readJsonFile($id . '.json');
        } catch (HddException $exception) {
            throw RepositoryException::buildForInfrastructure($exception);
        } catch (InvalidDataException $exception) {
            throw RepositoryException::buildForInfrastructure($exception);
        }

        $result = [];

        foreach ($data->brands as $brand) {
            $result[] = BrandFactory::buildFromJson((object) $brand);
        }

        return $result;
    }

    /**
     * @param string $name
     *
     * @return Brand[]
     * @throws RepositoryException
     */
    public function loadCollectionByName(string $name): array
    {
        $files = $this->reader->getAvailableFiles();

        foreach ($files as $file) {
            try {
                $data = $this->reader->readJsonFile($file);
            } catch (HddException $exception) {
                throw RepositoryException::buildForInfrastructure($exception);
            } catch (InvalidDataException $exception) {
                throw RepositoryException::buildForInfrastructure($exception);
            }
            if (property_exists($data, 'collection') && $name === $data->collection) {
                $result = [];

                foreach ($data->brands as $brand) {
                    $result[] = BrandFactory::buildFromJson((object) $brand);
                }

                return $result;
            } else {
                continue;
            }
        }

    }
}