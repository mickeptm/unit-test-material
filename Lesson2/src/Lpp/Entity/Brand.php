<?php
declare(strict_types=1);
namespace App\Lpp\Entity;

/**
 * Represents a single brand in the result.
 *
 */
class Brand implements OrderableInterface
{
    /**
     * Name of the brand
     *
     * @var string
     */
    private $brand;

    /**
     * Brand's description
     * 
     * @var string
     */
    private $description;

    /**
     * Unsorted list of items with their corresponding prices.
     * 
     * @var Item[]
     */
    private $items = [];

    /**
     * Brand constructor.
     *
     * @param string $name
     * @param string $description
     * @param array $items
     */
    public function __construct(string $name, string $description, array $items)
    {
        $this->brand = $name;
        $this->description = $description;
        $this->items = $items;
    }

    /**
     * Returns order key
     *
     * @return string
     */
    public function getOrderKey() : string
    {
        return $this->brand;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return Item[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
