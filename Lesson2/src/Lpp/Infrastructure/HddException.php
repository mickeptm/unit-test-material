<?php
declare(strict_types=1);
namespace App\Lpp\Infrastructure;

use \Exception;

/**
 * HddException - throwns any time that we encounter problems with HardDrive reading
 *
 * @package App\Lpp\Infrastructure
 */
class HddException extends Exception
{
    CONST READ_CODE = 1001;
    CONST SCAN_CODE = 1002;

    /**
     * Named constructor for this exception for read problem
     *
     * @param string $file
     *
     * @return HddException
     */
    public static function buildCannotRead(string $file) : self
    {
        return new self('Cannot read file: ' . $file, self::READ_CODE);
    }

    /**
     * Named constructor for this exception for scan problem
     *
     * @param string $dir
     *
     * @return HddException
     */
    public static function buildCannotScan(string $dir) : self
    {
        return new self('Cannot scan dir' . $dir, self::SCAN_CODE);
    }
}