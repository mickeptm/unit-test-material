<?php
declare(strict_types=1);
namespace App\Lpp\Service;

use App\Lpp\Entity\Brand;
use App\Lpp\Entity\Item;

/**
 * Class UnorderedBrandService
 *
 * @package App\Lpp\Service
 */
class UnorderedBrandService implements BrandServiceInterface
{
    /**
     * @var ItemServiceInterface
     */
    private $itemService;

    /**
     * @param ItemServiceInterface $itemService
     */
    public function __construct(ItemServiceInterface $itemService) {
       $this->itemService = $itemService;
    }

    /**
     * @param string $collectionName Name of the collection to search for.
     *
     * @return Brand[]
     */
    public function getBrandsForCollection(string $collectionName) : array {
        return $this->itemService->getResultsForCollectionName($collectionName);
    }

    /**
     * @deprecated load ItemService via constructor ONLY
     * @param ItemServiceInterface $itemService
     *
     * @return void
     */
    public function setItemService(ItemServiceInterface $itemService) : void {
        trigger_error('setItemService is abandoned and will be removed in next release, its advised not to use it', E_USER_NOTICE);
        $this->itemService = $itemService;
    }

    /**
     * @param string $collectionName Name of a collection to search for.
     *
     * @return Item[]
     */
    public function getItemsForCollection(string $collectionName) : array
    {
        $brands = $this->itemService->getResultsForCollectionName($collectionName);
        $result = [];
        foreach($brands as $brand) {
            foreach ($brand->getItems() as $item) {
                $result[] = $item;
            }
        }

        return $result;
    }
}
