<?php
declare(strict_types=1);
namespace App\Lpp\Entity;

use App\Lpp\Entity\Validation\UrlValidator;
use App\Lpp\Entity\Validation\ValidationException;

/**
 * Represents a single item from a search result.
 * 
 */
class Item implements OrderableInterface
{
    /**
     * Name of the item
     *
     * @var string
     */
    private $name;

    /**
     * Url of the item's page
     * 
     * @var string
     */
    private $url;

    /**
     * Unsorted list of prices received from the 
     * actual search query.
     * 
     * @var Price[]
     */
    private $prices = [];

    /**
     * Item constructor.
     *
     * @param string $name
     * @param string $url
     * @param array $prices
     *
     * @throws ValidationException
     */
    public function __construct(string $name, string $url, array $prices)
    {
        if (!UrlValidator::validateUrl($url)) {
            throw ValidationException::buildWithError('invalid url - ' . $url);
        }

        $this->name = $name;
        $this->url = $url;
        $this->prices = $prices;
    }

    /**
     * Returns order key
     *
     * @return string
     */
    public function getOrderKey() : string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return Price[]
     */
    public function getPrices(): array
    {
        return $this->prices;
    }
}
