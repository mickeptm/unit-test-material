<?php
declare(strict_types=1);

namespace App\Controller;

use App\Command\CreateUserCommand;
use App\Command\UpdateUserCommand;
use App\Repository\UserRepository;
use App\Service\GetUsersService;
use App\Service\ManageUsersService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CRUDController
{
    /**
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        global $database;

        $command = CreateUserCommand::buildFromRequest($request);
        $service = new ManageUsersService(new UserRepository($database));
        $user = $service->createUser($command);

        return new Response($user->toJson());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function update(Request $request): Response
    {
        global $database;

        $command = UpdateUserCommand::buildFromRequest($request);
        $service = new ManageUsersService(new UserRepository($database));
        $user = $service->updateUser($command);

        return new Response($user->toJson());

    }

    /**
     * @param Request $request
     * @return Response
     */
    public function read(Request $request): Response
    {
        global $database;

        $service = new GetUsersService(new UserRepository($database));
        $user = $service->getUser((int) $request->query->get('id'));

        return new Response($user->toJson());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function delete(Request $request): Response
    {
        global $database;

        $service = new ManageUsersService(new UserRepository($database));
        $user = $service->deleteUser((int) $request->query->get('id'));

        return new Response($user ? 'true' : 'false');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function list(Request $request): Response
    {
        global $database;

        $service = new GetUsersService(new UserRepository($database));
        $users = $service->getUsersCollection();
        $data = '[';
        foreach($users as $user) {
            $data .= $user->toJson() . ',';
        }

        $data = substr($data,0,-1);

        $data .= ']';

        return new Response($data);
    }
}