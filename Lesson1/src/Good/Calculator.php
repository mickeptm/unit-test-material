<?php

namespace Xzsoftware\Good;

use Xzsoftware\Math;

class Calculator
{
    private $memory = null;

    private $mathLib = null;

    /**
     * Calculator constructor.
     * @param Math $mathLib
     */
    public function __construct(Math $mathLib)
    {
        $this->mathLib = $mathLib;
    }

    /**
     * @param $value
     * @throws \Exception
     */
    public function saveValueToMemory($value)
    {
        if (!is_int($value)) {
            throw new \Exception('Value saved to memory must be integer!');
        }

        $this->memory = $value;
    }

    /**
     * @return null
     */
    public function readValueFromMemory()
    {
        return $this->memory;
    }

    /**
     * @param $a
     * @param null $b
     * @return int
     * @throws \Exception
     */
    public function add($a, $b = null)
    {
        if (is_null($b)) {
            $b = $this->readValueFromMemory();
        }

        if (!is_int($a) || !is_int($b)) {
            throw new \Exception('Values must be integers');
        }

        return $a + $b;
    }

    /**
     * @param $a
     * @param null $b
     * @return int
     * @throws \Exception
     */
    public function subtract($a, $b = null)
    {
        if (is_null($b)) {
            $b = $this->readValueFromMemory();
        }

        if (!is_int($a) || !is_int($b)) {
            throw new \Exception('Values must be integers');
        }

        return $a - $b;
    }

    /**
     * @param $value
     * @param null $power
     * @return int
     */
    public function power($value, $power = null)
    {
        if (is_null($power)) {
            $power = $this->readValueFromMemory();
        }

        if (!is_int($value) || !is_int($power)) {
            throw new \Exception('Values must be integers');
        }

        return $this->mathLib->power($value, $power);
    }

    /**
     * @param $value
     * @param null $modulo
     * @return int
     * @throws \Exception
     */
    public function modulo($value, $modulo = null)
    {
        if (is_null($modulo)) {
            $modulo = $this->readValueFromMemory();
        }

        if (!is_int($value) || !is_int($modulo)) {
            throw new \Exception('Values must be integers');
        }

        return $this->mathLib->modulo($value, $modulo);
    }
}