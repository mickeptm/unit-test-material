<?php
declare(strict_types=1);
namespace App\Lpp\Service;

use App\Lpp\Entity\Brand;
use App\Lpp\Repository\ItemRepositoryInterface;

/**
 * ItemServiceRepository implementation
 * It uses ItemRepository. Due to lack of any real service logic in this project, it simply runs repository reading
 *
 * @package App\Lpp\Service
 */
class ItemService implements ItemServiceInterface
{
    /** @var ItemRepositoryInterface */
    private $repository;

    /**
     * ItemService constructor.
     *
     * @param ItemRepositoryInterface $repository
     */
    public function __construct(ItemRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * This method should read from a datasource
     * and should return an unsorted list of brands found in the datasource.
     *
     * @param int $collectionId
     *
     * @return Brand[]
     */
    public function getResultForCollectionId(int $collectionId) : array
    {
        return $this->repository->loadCollection($collectionId);
    }

    /**
     * @param string $collectionName
     *
     * @return Brand[]
     */
    public function getResultsForCollectionName(string $collectionName): array
    {
        return $this->repository->loadCollectionByName($collectionName);
    }
}