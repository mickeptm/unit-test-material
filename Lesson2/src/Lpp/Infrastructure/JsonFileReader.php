<?php
declare(strict_types=1);
namespace App\Lpp\Infrastructure;

use stdClass;


/**
 * JsonFileReader - reads files from hdd that are in json format and translate them to php stdClass objects
 *
 * @package App\Lpp\Infrastructure
 */
class JsonFileReader
{
    /** @var string */
    private $path;

    /**
     * JsonFileReader constructor.
     *
     * @param string $readPath
     */
    public function __construct(string $readPath)
    {
        $this->path = $readPath;
    }

    /**
     * @param string $fileName
     *
     * @return stdClass
     * @throws HddException
     * @throws InvalidDataException
     */
    public function readJsonFile(string $fileName) : stdClass
    {
        return $this->readJson($this->readFile($fileName));
    }

    /**
     * @param string $fileName
     *
     * @return string
     * @throws HddException
     */
    private function readFile(string $fileName) : string
    {
        $data = file_get_contents($this->path . $fileName);

        if (!is_string($data) || empty($data)) {
            throw HddException::buildCannotRead($fileName);
        }

        return $data;
    }

    /**
     * @param string $string
     *
     * @return stdClass
     * @throws InvalidDataException
     */
    private function readJson(string $string) : stdClass
    {
        $jsonData = json_decode($string, true);

        if (!is_array($jsonData)) {
            throw InvalidDataException::buildForInvalidJson();
        }

        return (object) $jsonData;
    }

    /**
     * @return array
     * @throws HddException
     */
    public function getAvailableFiles() : array
    {
        $result = scandir($this->path);

        if (!is_array($result)) {
            throw HddException::buildCannotScan($this->path);
        }

        foreach ($result as $key => $file) {
            if (strlen($file) < 4) {
                unset($result[$key]);
            }
        }

        return $result;
    }
}