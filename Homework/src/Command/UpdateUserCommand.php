<?php
declare(strict_types=1);

namespace App\Command;

use App\DTO\User;
use Symfony\Component\HttpFoundation\Request;

class UpdateUserCommand extends User
{
    public function __construct(int $id, string $name, string $email)
    {
        parent::__construct($name, $email, $id);
    }

    /**
     * @param Request $request
     * @return UpdateUserCommand
     */
    public static function buildFromRequest(Request $request): self
    {
        return new self(
            (int) $request->request->get('id'),
            (string) $request->request->get('name', ''),
            (string) $request->request->get('email', '')
        );

    }
}
