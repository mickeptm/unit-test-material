<?php
declare(strict_types=1);
namespace App\Lpp\Entity;

use DateTime;

/**
 * Represents a single price from a search result
 * related to a single item.
 * 
 */
class Price
{
    /**
     * Description text for the price
     * 
     * @var string
     */
    private $description;

    /**
     * Price in euro
     * 
     * @var int
     */
    private $priceInEuro;

    /**
     * Warehouse's arrival date (to)
     *
     * @var \DateTime
     */
    private $arrivalDate;

    /**
     * Due to date,
     * defining how long will the item be available for sale (i.e. in a collection)
     *
     * @var \DateTime
     */
    private $dueDate;

    /**
     * Price constructor.
     *
     * @param string $description
     * @param int $price
     * @param DateTime $arrival
     * @param DateTime $due
     */
    public function __construct(string $description, int $price, DateTime $arrival, DateTime $due)
    {
        $this->description = $description;
        $this->priceInEuro = $price;
        $this->arrivalDate = $arrival;
        $this->dueDate = $due;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getPriceInEuro(): int
    {
        return $this->priceInEuro;
    }

    /**
     * @return DateTime
     */
    public function getArrivalDate(): DateTime
    {
        return $this->arrivalDate;
    }

    /**
     * @return DateTime
     */
    public function getDueDate(): DateTime
    {
        return $this->dueDate;
    }
}
