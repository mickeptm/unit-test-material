<?php
declare(strict_types=1);
namespace App\Lpp\Application;

use App\Lpp\Service\BrandServiceFactory;

/**
 * BrandApplicationService class that executes logic based on given commands
 * Keeps logic separated from Controller and presentation layer, can be used multiple times
 *
 * @package App\Lpp\Application
 */
class BrandApplicationService
{
    /** @var BrandServiceFactory */
    private $brandServiceFactory;

    /**
     * BrandApplicationService constructor.
     *
     * @param BrandServiceFactory $factory
     */
    public function __construct(BrandServiceFactory $factory)
    {
        $this->brandServiceFactory = $factory;
    }

    /**
     * For usage in controllers, returns Brands as string specified by given command
     *
     * @param BrandCommand $command
     *
     * @return string
     */
    public function getBrandsString(BrandCommand $command) : string
    {
        $brandService = $this->brandServiceFactory->getBrandService($command->getType());

        $items = $brandService->getItemsForCollection($command->getCollectionName());
        $i = 1;
        $result = '';
        foreach($items as $item) {
            $result .= $i . '. ' . $item->getName() . '; ' . $item->getUrl() . "\n";
            $i++;
        }

        return $result;
    }

    /**
     * For usage in controllers, returns items as string specified by given command
     *
     * @param BrandCommand $command
     *
     * @return string
     */
    public function getItemsString(BrandCommand $command) : string
    {
        $brandService = $this->brandServiceFactory->getBrandService($command->getType());
        $brands = $brandService->getBrandsForCollection($command->getCollectionName());

        $i = 1;
        $result = '';
        foreach($brands as $brand) {
            $result .= $i . '. ' . $brand->getBrand() . '; ' . $brand->getDescription() . "\n";
            $i++;
        }

        return $result;
    }
}